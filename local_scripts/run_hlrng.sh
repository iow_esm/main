user_at_dest=$1
dest_folder=$2
prepare_before_run=${3:-false}
setup=${4:-""}

python_module="miniconda3/22.11.1"

######################################## TODO: to be removed when all packages are globally available
activate_iow_esm=$(cat << eof
if [ \`grep "# >>> conda initialize >>>" ~/.bashrc | wc -l\` -eq 0 ]; then
    conda init bash
fi
source ~/.bashrc
if [ \`conda info --envs | grep iow_esm | wc -l\` -eq 0 ]; then 
    echo "Creating Conda environment 'iow_esm' using environment.yml..."
    conda env create --name iow_esm --file environment.yml
else
    conda activate iow_esm
fi
eof
)

python_module+="; eval '${activate_iow_esm}'"
########################################

if [ ${prepare_before_run} == true ]; then
	echo ssh -t $user_at_dest "module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_mappings.py ${setup}"
	ssh -t $user_at_dest "module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_mappings.py ${setup}"
fi

echo ssh -t $user_at_dest "module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_jobscript.py ${setup}"
ssh -t $user_at_dest "module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_jobscript.py ${setup}"

if ssh -t $user_at_dest "ls ${dest_folder}/input/${setup}/ENSEMBLE_MEMBERS > /dev/null 2>&1"
then  # ensemble run
	# download ensemble members for reference
	echo "Downloading ENSEMBLE_MEMBERS for local reference."
	rsync -ruv ${user_at_dest}:${dest_folder}/input/${setup}/ENSEMBLE_MEMBERS .

	# loop over the file remotely
	ssh -T $user_at_dest <<- EOF
	cd $dest_folder/scripts/run
	while read ensembleMember
	do
		ensembleSetup="\$(basename "\${ensembleMember}")"
		sbatch jobscript_\${ensembleSetup}
	done <$dest_folder/input/$setup/ENSEMBLE_MEMBERS
	EOF
else  # single run
	# run standard if there is no ensemble member file
	echo ssh -t $user_at_dest "cd $dest_folder/scripts/run; sbatch jobscript_${setup}"
	ssh -t $user_at_dest "cd $dest_folder/scripts/run; sbatch jobscript_${setup}"
fi
