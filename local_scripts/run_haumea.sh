user_at_dest=$1
dest_folder=$2
prepare_before_run=${3:-false}
setup=${4:-""}

python_module="miniconda3/4.8.2"

if [ ${prepare_before_run} == true ]; then
	echo ssh -t $user_at_dest "source ~/.bash_profile; module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_mappings.py"
	ssh -t $user_at_dest "source ~/.bash_profile; module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_mappings.py"
fi

echo ssh -t $user_at_dest "source ~/.bash_profile; module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_jobscript.py"
ssh -t $user_at_dest "source ~/.bash_profile; module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_jobscript.py"

if ssh $user_at_dest test ${dest_folder}/input/${setup}/ENSEMBLE_MEMBERS \> /dev/null 2\>\&1
then  # ensemble run
	# download ensemble members for reference
	echo "Downloading ENSEMBLE_MEMBERS for local reference."
	rsync -ruv ${user_at_dest}:${dest_folder}/input/${setup}/ENSEMBLE_MEMBERS .

	# loop over the file remotely
	ssh -T $user_at_dest <<- EOF
	cd $dest_folder/scripts/run
	source ~/.bash_profile
	while read ensembleMember
	do
		ensembleSetup="\$(basename "\${ensembleMember}")"
		sbatch jobscript_\${ensembleSetup}
	done <$dest_folder/input/$setup/ENSEMBLE_MEMBERS
	EOF
else  # single run
	# run standard if there is no ensemble member file
	echo ssh -t $user_at_dest "cd $dest_folder/scripts/run; source ~/.bash_profile; sbatch jobscript"
	ssh -t $user_at_dest "cd $dest_folder/scripts/run; source ~/.bash_profile; sbatch jobscript"
fi
