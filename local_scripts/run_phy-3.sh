user_at_dest=$1
dest_folder=$2
prepare_before_run=${3:-false}
setup=${4:-""}

python_module="miniconda3/4.8.2"

######################################## TODO: to be removed when all packages are globally available
activate_iow_esm=$(cat << eof
if [ \`grep "# >>> conda initialize >>>" ~/.bashrc | wc -l\` -eq 0 ]; then
    conda init bash
fi
source ~/.bashrc
if [ \`conda info --envs | grep iow_esm | wc -l\` -eq 0 ]; then 
    echo y | conda create --name iow_esm
    conda activate iow_esm 
    echo y | conda install conda=24.5.0
    echo y | conda install netCDF4
    echo y | conda install xarray
    echo y | conda install shapely
    echo y | conda install scipy
    echo y | conda install matplotlib
else
    conda activate iow_esm
fi
eof
)

python_module+="; eval '${activate_iow_esm}'"
########################################

if [ ${prepare_before_run} == true ]; then
	echo ssh -t $user_at_dest "module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_mappings.py ${setup}"
	ssh -t $user_at_dest "module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_mappings.py ${setup}"
fi

echo ssh -t $user_at_dest "module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_jobscript.py ${setup}"
ssh -t $user_at_dest "module load ${python_module}; cd $dest_folder/scripts/prepare; python3 ./create_jobscript.py ${setup}"

if ssh $user_at_dest test ${dest_folder}/input/${setup}/ENSEMBLE_MEMBERS \> /dev/null 2\>\&1
then  # ensemble run
	# download ensemble members for reference
	echo "Downloading ENSEMBLE_MEMBERS for local reference."
	rsync -ruv ${user_at_dest}:${dest_folder}/input/${setup}/ENSEMBLE_MEMBERS .

	# loop over the file remotely
	ssh -T $user_at_dest <<- EOF
	cd $dest_folder/scripts/run
	while read ensembleMember
	do
		ensembleSetup="\$(basename "\${ensembleMember}")"
		chmod u+x jobscript_\${ensembleSetup}
		(nohup ./jobscript_\${ensembleSetup} >& nohup.out.\$(date | tr ':' '.' | sed s/' '/'_'/g) &)
		sleep 2
	done <$dest_folder/input/$setup/ENSEMBLE_MEMBERS
	EOF
else  # single run
	# run standard if there is no ensemble member file
	echo ssh -t $user_at_dest "cd $dest_folder/scripts/run; chmod u+x jobscript_${setup}; (nohup ./jobscript_${setup} &> nohup.out.\$(date | tr ':' '.' | sed s/' '/'_'/g) &); sleep 2"
	ssh -t $user_at_dest "cd $dest_folder/scripts/run; chmod u+x jobscript_${setup}; (nohup ./jobscript_${setup} >& nohup.out.\$(date | tr ':' '.' | sed s/' '/'_'/g) &); sleep 2"
fi
