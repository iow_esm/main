# -*- coding: utf-8 -*-
"""
Created on Thu Sep 16 14:55:17 2021
Modified on Mon Nov 25 09:30:00 2024

@author: Sven Karsten, Georg Sebastian Voelker
"""

import backend
import platform
import subprocess
import glob
from functools import partial
from typing import Callable

from frontend.decorators import IowEsmFrontendDecorator
from threading import Thread, Event

import os

class IowEsmInterfaces:
    """Class summarizing the interfaces for IOW-ESM configuration, execution controlling, and post processing.
    """
    def __init__(self, eh: backend.error_handler.IowEsmErrorHandler):
        """Interface to backend functionality of IOW-ESM.

        Args:
            eh (IowEsmErrorHandler): error handler, attached to the interface
        """
        self.eh = eh
        
        if platform.system() == "Linux":
            self.bash = "`which bash`"
        if platform.system() == "Windows":
            self.bash = "\"C:\\msys64\\usr\\bin\\env.exe\" MSYSTEM=MINGW64 /bin/bash"
        elif platform.system() == "Darwin":  # macOS
            self.bash = "`which bash`"
            
        self.shell_cmd_thread = Thread()
        self.cancel_cmd = Event()
        self.cmd_finished = Event()
        self.output = ""
    
    @IowEsmFrontendDecorator()
    def mark_cmd_finished(self) -> None:
        """Marks the current command as finished (interface execution handler).
        """
        self.cmd_finished.set()
        self.cmd_finished.clear()
    
    @IowEsmFrontendDecorator()
    def prepare_shell_cmd(self, cmd: str) -> Callable:
        """Prepare a shell command as a partial of 'subprocess.Popen'.

        Args:
            cmd (str): shell command to be executed

        Returns:
            Callable: partial of 'subprocess.Popen' which executes the command upon calling it
        """
        
        self.eh.report_error('intfc', 'info', f'Preparing execution of {cmd}')

        # Change FB 20.12.2024: Too much fstring gets complicated       
        replace_match = backend.globals.root_dir.replace('\\', '/')
        execution_cmd = f'{self.bash} -l -c "cd {replace_match}; {cmd}"'

#        execution_cmd = f'{self.bash} -l -c "cd {backend.globals.root_dir.replace("\\","/")}; {cmd}"'
        pp = partial(
            subprocess.Popen,
            execution_cmd,
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            preexec_fn=os.setsid
        )
        
        return pp
    
    @IowEsmFrontendDecorator()
    def execution_handler(self, proc: subprocess.Popen, printing: bool) -> str:
        """Interface execution handler: Execute a subprocess blocking the interface.

        Args:
            proc (subprocess.Popen): process to be executed
            printing (bool): return stdout if set to True, not otherwise

        Returns:
            str: result returned from the process
        """
        if printing:
            self.eh.report_error('intfc', 'info', f'Starting execution')
        
        if self.shell_cmd_thread.is_alive():
            self.eh.report_error('intfc', 'warning', 'Another command is already running. Please wait.')
            return ''
        
        running = proc()
        
        while(running.poll() is None):
            if printing:
                line = running.stdout.readline()
                
                # if run in fg, stdout may be sent to the log directly
                self.eh.report_error('cmd', 'stdout', line)
            
        if printing:
            self.eh.report_error('intfc', 'info', '...done')
            
        self.output = running.stdout.read().decode("utf-8")
        
        self.mark_cmd_finished()

        return self.output
        
    
    @IowEsmFrontendDecorator()
    def cancel_shell_cmd(self) -> None:
        """Cancel the shell command if it is running.
        """
        
        if not  self.shell_cmd_thread.is_alive():
            self.eh.report_error('intfc', 'warning', 'No command is running.')
            return 
        
        self.eh.report_error('intfc', 'info', 'Canceling...')
        self.cancel_cmd.set()
    
    @IowEsmFrontendDecorator()
    def clone_origins(self, origins: backend.auxclasses.FormatText) -> None:
        """Clone all origins in given dictionary subclass, execute the command through the interface execution handler.

        Args:
            origins (backend.auxclasses.FormatText): dictionary with origins
        """
        cmd = "./clone_origins.sh"
        proc = self.prepare_shell_cmd(cmd)
        output = self.execution_handler(proc, printing=True)
        
        if output:
            self.eh.report_error('intfc', 'info', f'{cmd} returned {output}.')
        
        for ori in origins.keys():
            if glob.glob(backend.globals.root_dir + "/" + ori + "/.git") == []:
                self.eh.report_error(*backend.error_handler.IowEsmErrors.clone_origins)
                return
        
        self.eh.remove_from_log(*backend.error_handler.IowEsmErrors.clone_origins)
        
    
    @IowEsmFrontendDecorator()
    def clone_origin(self, origin: str) -> Callable:
        """Clone an origin, return a partial of 'suprocess.Popen' for the execution handler.

        Args:
            origin (str): name of origin

        Returns:
            Callable: partial of 'subprocess.Popen' to be given to the execution handler
        """
        
        cmd = f'./clone_origins.sh {origin}'
        proc = self.prepare_shell_cmd(cmd)
        
        return proc
    
    @IowEsmFrontendDecorator()
    def check_origin(self, origin: str) -> None:
        """Check if the origin is cloned.

        Args:
            origin (str): name of origin from ORIGINS config
        """
        
        if not glob.glob(backend.globals.root_dir + f'/{origin}/.git'):
            self.eh.report_error(*backend.error_handler.IowEsmErrors.clone_origins)
            return
        
        self.eh.remove_from_log(*backend.error_handler.IowEsmErrors.clone_origins)
        
        
    
    @IowEsmFrontendDecorator()
    def check_for_available_inputs(self, destinations: backend.auxclasses.FormatText, current: str) -> list:
        """Checks for inputs that are available on the given destination.

        Args:
            destinations (backend.auxclasses.FormatText): dictionary of destinations
            current (str): current destination name

        Returns:
            list: list of inputs which are available at the destination
        """
        
        # do not check if destination dict is empty
        if destinations:
            file_content = ""
            user_at_host, path = destinations[current].split(":")
            cmd = f'ssh {user_at_host} "if [ -d {path}/input ]; then ls {path}/input; fi"'
            self.eh.report_error('intfc', 'info', 'Checking for available inputs...')
            sp = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
            file_content = sp.stdout.read().decode('utf-8') 

            if file_content == "":
                self.eh.report_error(
                    'intfc', 'warning',
                    f'No setup deployed (yet) at destination {current}.'
                )
                return []

            if 'global_settings.py' in file_content:
                return ['input']
            else:
                inputs = file_content.split('\n')
                inputs.remove('')
                
                self.eh.report_error('intfc', 'info', f'Found deployed inputs: {inputs}')
                return inputs
        else:
            self.eh.report_error('intfc', 'info', f'Destinations not defined')
            return []
            

    # TODO: Implement adding targets into frontend
    # @IowEsmFrontendDecorator()
    # def add_target(self):
    #     cmd = "./add_target.sh " + self.frontend.entries["add_target_keyword"].get() + " " + self.frontend.entries["add_target_class"].get()
    #     self.execute_shell_cmd(cmd, blocking=False)

    @IowEsmFrontendDecorator()
    def sync_now(self, destination: str, sync_destination: str) -> Callable:
        """Sync the model output from a destination to the target destination.

        Args:
            destination (str): destination to sync from
            sync_destination (str): destination to sync to

        Returns:
            callable: partial of 'subprocess.Popen' to be given to the execution handler
        """
        cmd = f'./sync.sh {destination} {sync_destination}'
        proc = self.prepare_shell_cmd(cmd)
        
        return proc
    
    @IowEsmFrontendDecorator()
    def build_origin(self, origin: str, destination: str, build_config: str) -> Callable:
        """Build origin of given name on a given destination with a given build config.

        Args:
            origin (str): name of origin / target
            destination (str): destination to build on
            build_config (str): build config to use

        Returns:
            Callable: partial of 'subprocess.Popen' to be given to the execution handler
        """
        # Change 20.12.2024 FB, complicated fstring 
        fixed_origin = origin.replace('\\', '/')
        cmd = f'cd {fixed_origin} ; ./build.sh {destination} {build_config}'
        #cmd = f'cd {origin.replace("\\","/")} ; ./build.sh {destination} {build_config}'
        proc = self.prepare_shell_cmd(cmd)
        
        return proc
        
    @IowEsmFrontendDecorator()
    def build_origins(self, destination: str, build_config: str) -> Callable:
        """Build all origins on given destination with given build config.

        Args:
            destination (str): name of destination
            build_config (str): build config

        Returns:
            Callable: partial of 'subprocess.Popen' to be given to the execution handler
        """
        if not destination:
            self.eh.report_error(*backend.error_handler.IowEsmErrors.destination_not_set)
            return False
        
        cmd =f'./build.sh {destination} {build_config}'
        proc = self.prepare_shell_cmd(cmd)
        
        return proc
    
    @IowEsmFrontendDecorator()
    def build_origins_first_time(self, origins: backend.auxclasses.FormatText, destination: str, build_config: str) -> bool:
        """Builds the build origins the first time and log them in the LAST_BUILD_* file.

        Args:
            origins (backend.auxclasses.FormatText): dictionary of origins
            destination (str): name of destination
            build_config (str): build config

        Returns:
            bool: returns True if successful and False otherwise
        """
        
        # try to build the origins
        if not destination:
            self.eh.report_error(*backend.error_handler.IowEsmErrors.destination_not_set)
            return False
        
        cmd = f'./build.sh {destination} {build_config}'
        proc = self.prepare_shell_cmd(cmd)
        self.execution_handler(proc, printing=True)
        
        # if build has happened a file has been created, if not log error
        last_build_file = backend.globals.root_dir + \
            f'/LAST_BUILD_{destination}_{build_config.split(" ")[0]}'
        
        if not glob.glob(last_build_file):
            self.eh.report_error(*backend.error_handler.IowEsmErrors.build_origins_first_time)
            return False

        # in the file all origins should appear by name
        with open(last_build_file, 'r') as file:
            file_content = file.read()
        file.close()
        
        for ori in origins:
            if ori.split("/")[-1] not in file_content:
                self.eh.report_error(*backend.error_handler.IowEsmErrors.build_origins_first_time)
                return False

        # if everything went fine we can remove old errors
        self.eh.remove_from_log(*backend.error_handler.IowEsmErrors.build_origins_first_time)
    
        return True
    
    @IowEsmFrontendDecorator()
    def get_setups_info(self, setups: backend.auxclasses.FormatText) -> list:
        """Get infos about available setups.

        Args:
            setups (backend.auxclasses.FormatText): dictiopnary of supplied setups

        Returns:
            list: list of dictionaries with infos
        """
        
        infos = []
        for setup in setups:
            file_content = ''
            
            if ':' in setups[setup]:
                user_at_host, path = setups[setup].split(':')
                cmd = f'ssh {user_at_host} "if [ -f {path}/SETUP_INFO ]; then cat {path}/SETUP_INFO; fi"'
                self.eh.report_error('intfc', 'info', f'Executing {cmd}')
                sp = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
                file_content = sp.stdout.read().decode('UTF-8')

            infos.append(backend.auxclasses.FormatText(
                path=f'{setups[setup]}/SETUP_INFO',         # file path
                name=f'setup info: {setup}/SETUP_INFO',     # obj name
                parent=self,                                # may be reset by frontend
                post_edit=self._dummy,                      # post edit hook, may be reset by frontend
                text=file_content,                          # set text without pulling from file source
            ))

        return infos

    @IowEsmFrontendDecorator()
    def archive_setup(self, destination: str, setups: list, archive: str) -> bool:
        """Archive setup at destination.

        Args:
            destination (str): name of destination
            setups (list): list of setups, the last will be used as base
            archive (str): archive destination

        Returns:
            bool: returns True if successful, False otherwise
        """
        if not destination:
            self.eh.report_error(*backend.error_handler.IowEsmErrors.destination_not_set)
            return False
        
        if len(setups) > 1:
            self.eh.report_error('intfc', 'warning', 'More than one setup is selected. Take the last one as base.')
        
        cmd = f'./archive_setup.sh {destination} {setups[-1]} {archive}'
        proc = self.prepare_shell_cmd(cmd)
        self.execution_handler(proc, printing=True)

        return True
    
    @IowEsmFrontendDecorator()
    def deploy_setups(self, destination: str, setups: list) -> list:
        """Deploy multiple setups to the specified destination.

        Args:
            destination (str): name of destination
            setups (list): list of setups

        Returns:
            list: returns a list of callable processes (per setup)
        """
        if not destination:
            self.eh.report_error(*backend.error_handler.IowEsmErrors.destination_not_set)
            return self._dummy
        
        procs = []
        for setup in setups:
            cmd = f'./deploy_setups.sh {destination} {setup}'
            
            procs.append(self.prepare_shell_cmd(cmd))
            # self.execution_handler(proc, printing=True)
    
        return procs
    
    @IowEsmFrontendDecorator()
    def deploy_setups_first_time(self, destination: str) -> None:
        """Deploy setups at the destination for the first time.

        Args:
            destination (str): name of destination
        """
        if not self.deploy_setups():
            return
        
        last_setups_file = f'{backend.globals.root_dir}/LAST_DEPLOYED_SETUPS_{destination}'
        
        if not glob.glob(last_setups_file):
            self.eh.report_error(*backend.error_handler.IowEsmErrors.deploy_setups_first_time)
            return

        self.eh.remove_from_log(*backend.error_handler.IowEsmErrors.deploy_setups_first_time)
        

    @IowEsmFrontendDecorator()
    def run(self, destination: str, sync_destination: str,  prepare: bool, inputs: list) -> Callable:
        """Run the model with given parameters. 

        Args:
            destination (str): name of destination
            sync_destination (str): name of destination to sync output to
            prepare (bool): flag for run preparation
            inputs (list): list of inputs to use

        Returns:
            Callable: partial of 'subprocess.Popen' to be given to the execution handler
        """
        if not destination:
            self.eh.report_error(*backend.error_handler.IowEsmErrors.destination_not_set)
            return partial(self._dummy)
        else:
        
            cmd = f'./run.sh {destination}'
            
            if prepare:
                cmd = f'{cmd} prepare-before-run'

            if sync_destination:
                cmd = f'{cmd} sync_to={sync_destination}'

            if len(inputs) < 1:
                self.eh.report_error('intfc', 'warning', 'No input folder selected or available. Abort.')
                return partial(self._dummy)

            if 'input' in inputs and len(inputs) > 1:
                self.eh.report_error(
                    'intfc', 'warning',
                    'Input folder name \"input\" is reserved and cannot be used with other input folder together. Abort.'
                )
                return partial(self._dummy)

            if 'input' in inputs:
                # if only input is available, this is the old default and this corresponds to no arguments
                inputs = [] 

            for input in inputs:
                cmd = f'{cmd} {input}'
                
            proc = self.prepare_shell_cmd(cmd)
            
            return proc

    @IowEsmFrontendDecorator()
    def _dummy(*args, **kwargs) -> None:
        """A dummy method.
        """
        pass