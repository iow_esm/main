import os
from datetime import datetime
import glob
import errno

class AttemptHandlerRunoff():
    """Class that handles your attempts.
    
    The name of the class is arbitrary

    Mandatory attributes: `next_attempt` which represents your next attempt, type must be convertable by str() function to string, typically a string or an integer, if all attempts are exhausted and you want to stop this must be set to None.
                                    
    Class can have arbitrarily more optional attributes. 
    However, all attributes must be serializable by the pickle library, see https://docs.python.org/3/library/pickle.html.
    The serialization into a file is necessary to store the state of the attempt handler over several jobs.
    IMPORTANT: If you want to start from scratch, you have to remove such files, which are sored as <run_name>_attempt_handler.obj in the root directory.

    Parameters can be arbitrary. In this example:

    :param root:        Path to the root directory
    :type root:         str     
        
    """
    
    def __init__(self, root, input_name, initial_start_date):  
    
        # initialize mandatory attribute self.next_attempt
        self.next_attempt = 1
    
        
        # optional arguments and members
        
        # it makes sense to memorize the root directory
        self.root = root     

        self.input_name = input_name
        self.initial_start_date = initial_start_date
        
        # our maximal number of attempts
        self.max_attempts = 1

        self.work_dir = self.root+"/input/"+self.input_name+"/MOM5_Baltic/prepare_runoff"

        required_files = [self.work_dir+"/*.pickle", self.work_dir+"/prepare_runoff.py", self.work_dir+"/configuration.py", self.work_dir+"/grid_spec.nc"]
        
        for file in required_files:
            if not glob.glob(file):
              raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), file)
        
    def prepare_attempt(self, **kwargs):
        r"""
        Mandatory method to prepare the attempt.
        
        Do whatever is necessary to set up the next attempt, e.g. manipulating input files.

        :Keyword Arguments:

        * **start_date** (*int*) --
          Start date of the current job in format YYYMMDD
        * **end_date** (*int*) --
          End date of the current job in format YYYMMDD  
                
        """     

        # you can use the keyword arguments
        start_date = kwargs["start_date"]
        end_date = kwargs["end_date"]

        if start_date == self.initial_start_date:
            return
        
        print("Peparing " + str(self.next_attempt) + " for start date " +  str(start_date) + " and end date " + str(end_date))

        old_work_dir = os.getcwd()
        os.chdir(self.work_dir)

        import configuration

        # Convert the date string to a datetime object
        date_object = datetime.strptime(str(start_date), "%Y%m%d")

        # Get the year and month
        year, month = date_object.year, date_object.month

        # Get the number of days in the given month using calendar
        #days_in_month = calendar.monthrange(year, month)[1]

        last_month = month - 1 # get the last month
        last_year = year
        if last_month < 1: last_year -= 1; last_month = 12 

        CCLM_output_dir = self.root+"/output/"+self.input_name+"_monthly/CCLM_Eurocordex/"+f"{last_year:4d}"+f"{last_month:02d}"+"01"

        for CCLM_variable in configuration.runoff_input:
            os.system("cp "+CCLM_output_dir+"/"+CCLM_variable+".nc .")
        
        import prepare_runoff

        prepare_runoff.prepare_runoff(str(start_date))

        result_file = "runoff.nc"
        if not glob.glob(result_file):
            print("Something went wrong! No "+result_file+" produced!")
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), result_file)
            
        # copy some prepared files to the actual input file
        MOM5_input_dir = self.root+"/input/"+self.input_name+"/MOM5_Baltic/RIVER_coupled/"+f"{year:4d}"+f"{month:02d}"+"01"
        os.system("mkdir -p "+MOM5_input_dir)
        os.system("cp "+result_file+" "+MOM5_input_dir+"/")
        
        os.chdir(old_work_dir)

        return

    def evaluate_attempt(self, crashed, **kwargs):
        r"""
        Mandatory method to evaluate the attempt.
        
        In this method the setting of the next_attempt should typically happen, e.g. incrementation.
        Important: If all attempts are exhausted, next_attempt must be set tot `None`.
        Important: If model has crashed, this function should return False otherwise following steps are ill-defined.

        :param crashed:         `True`, if the model has crashed, `False`, otherwise
        :type crashed:          bool   

        :Keyword Arguments:
        
        * **start_date** (*int*) --
          Start date of the current job in format YYYMMDD
        * **end_date** (*int*) --
          End date of the current job in format YYYMMDD          

        :return:                `True`, if attempt is accepted (work will be copied to output, hotstart folder is created), `False`, if attempt is not accepted (work will not be copied to output, no hotstart folder is created)      
        :rtype:                 bool
        """ 

        # you can use the keyword arguments
        start_date = kwargs["start_date"]
        end_date = kwargs["end_date"]
        print("Evaluating " + str(self.next_attempt) + " for start date " +  str(start_date) + " and end date " + str(end_date))

        # if the model has crashed, react here
        if crashed:
            
            # we have no attempts left, we should stop here
            if self.next_attempt == self.max_attempts:
                self.next_attempt = None
                return False
            
            # there are attempts left, go to the next set of input files 
            self.next_attempt += 1
            # throw away work of failed attempt (you might also store it somewhere for debugging)
            return False
            
        # if the model did succeed, we can go back to the previous input files
        if self.next_attempt > 1:
            self.next_attempt -= 1                
            
        return True
