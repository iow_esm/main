# HLRN uses Intel MPI and SLURM
mpi_run_command = 'mpirun -configfile mpmd_file'        # the shell command used to start the MPI execution described in a configuration file "mpmd_file"
                                                    # it may contain the following wildcards that will be replaced later:
                                                    #   _NODES_ total number of nodes
                                                    #   _CORES_ total number of cores to place threads on
                                                    #   _THREADS_ total number of mpi threads
                                                    #   _CORESPERNODE_ number of cores per node to use
                                                    #   Examples: Intel MPI: 'mpirun -configfile mpmd_file'
                                                    #             OpenMPI  : 'mpirun --app mpmd_file'
mpi_n_flag = '-n'                                       # the mpirun flag for specifying the number of tasks.
                                                    #   Examples: Intel MPI: '-n'
                                                    #             OpenMPI  : '-np'
bash_get_rank = 'my_id=${PMI_RANK}'                     # a bash expression that saves the MPI rank of this thread in the variable "my_id"
                                                    #   Examples: Intel MPI    : 'my_id=${PMI_RANK}'
                                                    #             OpenMPI+Slurm: 'my_id=${OMPI_COMM_WORLD_RANK}'
python_get_rank = 'my_id = int(os.environ["PMI_RANK"])' # a python expression that saves the MPI rank of this thread in the variable "my_id"
                                                    #   Examples: Intel MPI    : 'my_id = int(os.environ["PMI_RANK"])'
                                                    #             OpenMPI+Slurm: 'my_id = int(os.environ["OMPI_COMM_WORLD_RANK"])'

use_mpi_machinefile = ""         

def machinefile_line(node, ntasks):
    return ""

def get_node_list():
    return []

# command to resubmit new job when final date is not yet reached, is executed in scripts/run
resubmit_command = r"(sleep 5; nohup ./jobscript >& nohup.out.$(date | tr ':' '.' | sed s/' '/'_'/g) &)"
