from functools import partial, singledispatchmethod
from collections.abc import Callable
import numpy as np

import backend
import frontend

from PySide6.QtCore import (
    Qt, Slot
)
from PySide6.QtWidgets import (
    QGridLayout, QPlainTextEdit, QTableWidget,
    QTableWidgetItem, QToolButton, QWidget
)

from PySide6.QtGui import (
    QFontDatabase, QShortcut
)
    
class Editor(QWidget):
    def __init__(self, parent: frontend.main.IowEsmGui, content: object) -> None:
        super().__init__()
        self.setWindowTitle("IOW-ESM Data Editor")
        self.setGeometry(150, 150, 1000, 600)

        # read initial config
        self.content = content
        self.parent = parent
        
        # init buttons
        self.buttons = {}

        # build elements
        self._build_editor_window(self.content)
        self._button_constructor(
            name='save_button',
            descriptor=f"Save config to '{self.content.path}'",
            action=partial(self.save_to_file, self.content)
        )
        # TODO: implement check for modified content and add dialog to suggest saving
        self._button_constructor(
            name='close_button',
            descriptor='Close',
            action=self.close
        )

        # set editor layout
        self.layout = QGridLayout(self)
        self.layout.addWidget(self.editor, 0, 0, 1, 3)
        self.layout.addWidget(self.buttons['save_button'], 1, 0, 1, 2)
        self.layout.addWidget(self.buttons['close_button'], 1, 2, 1, 1)
        self.setLayout(self.layout)
        
        # set shortcuts
        shortcuts = {
            'close_1':  ('Esc',    self, self.close),
            'close_2':  ('Ctrl+W', self, self.close),
            'save':     ('Ctrl+S', self, partial(self.save_to_file, self.content))
        }
        for key in shortcuts:
            QShortcut(*shortcuts[key])
            
    @Slot()
    @singledispatchmethod
    def save_to_file(self, content: backend.auxclasses.FormatText) -> None:
        self.content.text = self.editor.toPlainText()
        self.content.save_content(text=self.content.text)
    
    @Slot()
    @save_to_file.register
    def _(self, content: backend.auxclasses.FormatTable) -> None:
        self.content.data = [[
            self.editor.item(row, column).text() if self.editor.item(row, column) else '' for column in range(self.editor.columnCount())
        ] for row in range(self.editor.rowCount())]
        self.content.save_content(data=self.content.data)

    def _button_constructor(self, name: str, descriptor: str, action: Callable) -> None:
        self.buttons[name] = QToolButton()
        frontend.main.init_widget(self.buttons[name], name)
        self.buttons[name].setText(descriptor)
        self.buttons[name].clicked.connect(action)
        self.buttons[name].setFixedHeight(30)

    @singledispatchmethod
    def _build_editor_window(self, content: object) -> None:
        print(type(content))
        raise NotImplementedError(f'Cannot build editor for {type(content)}')

    @_build_editor_window.register
    def _(self, content: backend.auxclasses.FormatText) -> None:
        # build as plain text editor
        self.editor = QPlainTextEdit(self.content.text)
        self.editor.setFont(QFontDatabase.systemFont(QFontDatabase.FixedFont))
        frontend.main.init_widget(self.editor, "text_editor")

    @_build_editor_window.register
    def _(self, content: backend.auxclasses.FormatTable) -> None:
        
        # build as table editor
        # to get better resizing use QTableView and an own table model
        self.editor = QTableWidget()
        frontend.main.init_widget(self.editor, "table_editor")
        self.editor.setRowCount(50)
        self.editor.setColumnCount(4)
        self.editor.setHorizontalHeaderLabels(['param', 'min', 'default', 'max'])
        self.editor.horizontalHeaderItem(Qt.AlignCenter)
        self.editor.setAlternatingRowColors(True)

        for column in range(self.editor.columnCount()):
            for row in range(self.editor.rowCount()):
                self.editor.setItem(row, column, QTableWidgetItem(self.content.data[row][column]))