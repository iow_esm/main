# Add a new model

## Create a new local repository

Create a new folder in `components` with the name of the new model, e.g. `components/MOM6`.
Initialize this folder a git repository or fork an existing repository in that folder.

Perform an initial commit of the current state with all necessary information in the commit message to reproduce this commit from scratch, i.e. from where is the code taken, at which date, the last commit message of the original repository, etc.


## Create a remote repository on 

Create a new repository for the model under https://git.io-warnemuende.de/iow_esm with the name `components.<model>`, where `<model>` is e.g. `mom6`. Please stick to the naming convention that the remote repository is written with small letters.

Push your initial commit to the remote repository.


## Add build script for necessary/available targets

Once the initial commit is saved for all times on the git server, you can start to make modifications.
The first is to write build scripts for the desired target machines, where the model should run.
Have a look at existing model components and their build scripts.

Essential is the `build.sh` script which is calling the `start_build_<target>.sh` that in turn is calling the `build_<target>.sh` script on the target machine with keyword `<target>`, see also [](getting_started:first_use).
It is easiest to copy build scripts from a similar model and then adjust compiler flags as needed.

Once you can successfully build the model commit the new build scripts to the repository.
You can now make further modifications of the code as needed for your purposes, e.g. build in a coupling interface.


## Add a model handler to the run scripts

It is now time to take care of runtime dependencies, i.e. creating work directories with necessary input files.
For that purpose you have to add model handler module to `scripts/run`.
The module must be named like the input folders that are used for the model, e.g. `model_handling_MOM6.py`for input folders like `MOM6_<domain>`, where domain can be arbitrary.

In this module you must implement a python class `class ModelHandler(model_handling.ModelHandlerBase)` that derives from the `ModelHandlerBase` class.
Your implementation has to overwrite the dummy methods of the base class such that your model can run, see existing implementations for hints how to do that.


## Add model repository to ORIGINS file

Add a line 





